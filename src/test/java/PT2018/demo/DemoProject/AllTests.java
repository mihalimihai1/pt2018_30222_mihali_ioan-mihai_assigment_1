package PT2018.demo.DemoProject;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(DerivareTest.class);
		suite.addTestSuite(InmultireTest.class);
		suite.addTestSuite(IntegrareTest.class);
		suite.addTestSuite(ScadereTest.class);
		suite.addTestSuite(TestFailure.class);
		//$JUnit-END$
		return suite;
	}

}
