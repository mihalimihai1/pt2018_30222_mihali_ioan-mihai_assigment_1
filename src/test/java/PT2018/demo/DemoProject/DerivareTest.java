package PT2018.demo.DemoProject;

import controllers.Operatii;
import junit.framework.TestCase;
import model.Monom;
import model.Polinom;

public class DerivareTest extends TestCase {
	public void testFailure() {
		Monom m1 = new Monom(1,1);
		Monom m2 = new Monom(2,2);
		Polinom a = new Polinom();
		Polinom rez;
		a.adaugaMonom(m1);
		a.adaugaMonom(m2);
		rez=Operatii.derivare(a);
		assertEquals(" +4.0X^1  +1.0X^0 ", rez.toString());
		}

}
