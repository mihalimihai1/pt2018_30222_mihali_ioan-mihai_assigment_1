package PT2018.demo.DemoProject;

import controllers.Operatii;
import junit.framework.TestCase;
import model.Monom;
import model.Polinom;

public class IntegrareTest extends TestCase {
	public void testFailure() {
		Monom m1 = new Monom(6,2);
		Monom m2 = new Monom(2,1);
		Polinom a = new Polinom();
		Polinom rez;
		a.adaugaMonom(m1);
		a.adaugaMonom(m2);
		rez=Operatii.integrare(a);
		assertEquals(" +2.0X^3  +1.0X^2 ", rez.toString());
		}

}
