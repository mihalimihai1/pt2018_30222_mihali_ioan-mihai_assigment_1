package model;

public class Monom implements Comparable<Monom> {
	public double coef;
	public int putere;

	public Monom(double coef, int putere) {
		this.coef=coef;
		this.putere=putere;
	}
	public Monom inmul_2(Monom a, Monom b) {
		Monom rez= new Monom(0,0);
		rez.coef=a.coef*b.coef;
		rez.putere=a.putere+b.putere;
		return(rez);
	}
	public int compareTo(Monom arg0) {
		return arg0.putere-this.putere;
	}
	public String toString() 
	{
		if ( this.putere > 0 )
			if(this.coef<0){
				return coef + "X^" + putere;
			}
			else return "+" + coef + "X^" + putere;
		else if(this.coef>0){
			return "+" + coef + "X^" + putere;}
		else return coef + "X^" + putere;
	}
}