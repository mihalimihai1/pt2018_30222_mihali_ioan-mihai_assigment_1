package view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

import controllers.Operatii;
import model.Polinom;

public class App{

	private Interfata view;
	private Polinom result;
	private String imput1="";
	private String imput2="";
	private Polinom p1=new Polinom();
	private Polinom p2=new Polinom();
	
	public App(Interfata v) {
		this.view = v;

		setupControl();
	}

	public void setupControl() {
		//Adunare
		view.b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				imput1=view.t1.getText();
				imput2=view.t2.getText();
				p1=Operatii.split(imput1);
				p2=Operatii.split(imput2);
				
				result = Operatii.adunare(p1, p2);
				Collections.sort(result.Monoame);
				view.t3.setText(result.toString());
			}
		});
		//Scadere
		view.b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				imput1=view.t1.getText();
				imput2=view.t2.getText();
				p1=Operatii.split(imput1);
				p2=Operatii.split(imput2);
				
				result = Operatii.scadere(p1, p2);
				Collections.sort(result.Monoame);
				view.t3.setText(result.toString());
			}
		});
		//Inmultire
		view.b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				imput1=view.t1.getText();
				imput2=view.t2.getText();
				p1=Operatii.split(imput1);
				p2=Operatii.split(imput2);
				
				result = Operatii.inmultire(p1, p2);
				Collections.sort(result.Monoame);
				view.t3.setText(result.toString());
			}
		});
		//Derivare pol1
		view.b4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				imput1=view.t1.getText();
				imput2=view.t2.getText();
				p1=Operatii.split(imput1);
				p2=Operatii.split(imput2);
				
				result = Operatii.derivare(p1);
				Collections.sort(result.Monoame);
				view.t3.setText(result.toString());
			}
		});
		//Integrare pol1
		view.b5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				imput1=view.t1.getText();
				imput2=view.t2.getText();
				p1=Operatii.split(imput1);
				p2=Operatii.split(imput2);
				
				result = Operatii.integrare(p1);
				Collections.sort(result.Monoame);
				view.t3.setText(result.toString());
			}
		});
		//Derivare pol2
		view.b6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				imput1=view.t1.getText();
				imput2=view.t2.getText();
				p1=Operatii.split(imput1);
				p2=Operatii.split(imput2);
				
				result = Operatii.derivare(p2);
				Collections.sort(result.Monoame);
				view.t3.setText(result.toString());
			}
		});
		//Integrare pol2
		view.b7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				imput1=view.t1.getText();
				imput2=view.t2.getText();
				p1=Operatii.split(imput1);
				p2=Operatii.split(imput2);
				
				result = Operatii.integrare(p2);
				Collections.sort(result.Monoame);
				view.t3.setText(result.toString());
			}
		});
		
	}
	public static void main(String[] args) {
	
		Interfata v=new Interfata();
		App c=new App(v);	
	}
}