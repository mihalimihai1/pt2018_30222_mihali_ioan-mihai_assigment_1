package controllers;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Monom;
import model.Polinom;


public class Operatii {
		
	public static Polinom adunare(Polinom a, Polinom b) {
		 Polinom rez = a.copy();
			for(Monom m: b.Monoame)
				rez.adaugaMonom(m);
			return rez;
	}
	public static Polinom scadere(Polinom a, Polinom b) {
		Polinom rez =a.copy();
		Polinom copya = b.copy();
		for(Monom m: copya.Monoame){
			m.coef = -(m.coef);
			rez.adaugaMonom(m);
		}	
		rez=rez.copy();
		return rez;
	}
	public static Polinom inmultire(Polinom a, Polinom b) {
		Polinom rez = new Polinom();
		Monom rez_m = new Monom(0,0);
		Polinom p1 = a.copy();
		Polinom p2 = b.copy();
		for(Monom m1: p1.Monoame)
			for(Monom m2: p2.Monoame)
			{
				rez_m=rez_m.inmul_2(m1, m2);
				rez.adaugaMonom(rez_m);
			}
		
		rez=rez.copy();
		return rez;
	}
	public static Polinom derivare(Polinom a) {
		Polinom rez = new Polinom();
		Polinom aux = a.copy();
		for(Monom x: aux.Monoame)
		{
			if(x.putere>0)
			{
				x.coef=x.coef*x.putere;
				x.putere--;
				rez.adaugaMonom(x);
			}
			else
				if(x.putere==0)
				{	
					x.coef=0;
					rez.adaugaMonom(x);
				}
		}
		return rez;
	}
	public static Polinom integrare(Polinom b) {
		Polinom rez= new Polinom();
		Polinom aux = b.copy();
		for(Monom x: aux.Monoame) 
			if(x.putere>=0)
			{
				x.putere++;
				x.coef=x.coef/x.putere;
				rez.adaugaMonom(x);
			}
		return rez;
	}
	public static Polinom split(String s)
	{
		Double[] sir = new Double[100];
		Double x;
		int cnt = 0;
		char y ;
		Polinom p = new Polinom();		
		String mydata = s;
		ArrayList<String> terms = new ArrayList<String>();
		Pattern pattern=Pattern.compile("(-?[0-9][0-9]*)");
		Matcher matcher=pattern.matcher(mydata);
		while (matcher.find())
				terms.add(matcher.group(1));
		for (String i:terms)
		{
			y=i.charAt(0);
			if (y=='-') 
			{
				i=i.replace('-', '0');
				x=Double.parseDouble(i);
				x*=-1;
			}
			else 
				x = Double.parseDouble(i);
			sir[cnt] = x;
			cnt++;
		}
		if ( cnt % 2 != 0 )
		{
			sir[cnt] = 0.0;
			cnt++;
		}
		for (int j=0;j<cnt;j=j+2)
			p.adaugaMonom(new Monom(sir[j],(sir[j+1]).intValue()));
		return p;
	}
}
